import React, { useState, useEffect } from 'react'
import loading from '../loading.gif'
import InfoBox from './InfoBox'

const Countdown = () => {

    const initialState = {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
        status: 'loading'
    }

    const [state, setState] = useState(initialState);

    const formatDate = (date) => {
        return date < 10 ? `0${date}` : date;
    }

    useEffect(() => {

        const countdown = setInterval(() => {

            const now = new Date();

            const toDay = new Date(`${now.getFullYear()}/${now.getMonth()}/${now.getDate()}`);

            let birthday = '';

            if (toDay >= new Date(`${now.getFullYear()}/4/14`)) {
                birthday = new Date(`14 may ${now.getFullYear() + 1}`);
                console.log('next year')
            } else {
                birthday = new Date(`14 may ${now.getFullYear()}`);
            }

            const left = birthday - now;

            const totalseconds = formatDate(Math.floor(left / 1000));
            const days = formatDate(Math.floor(totalseconds / 3600 / 24));
            const hours = formatDate(Math.floor(totalseconds / 3600 % 24));
            const minutes = formatDate(Math.floor(totalseconds / 60) % 60);
            const seconds = formatDate(totalseconds % 60);

            setState({
                ...state,
                days,
                hours,
                minutes,
                seconds,
                status: 'ready'
            });

            return clearInterval(countdown);
        }, 1000);

    }, [state])

    return (
        <>
            {
                state.status === 'loading'
                    ? (
                        <div className="container mt-5">
                            <div className="row justify-content-center mt-5">
                                <div className="col-md-3 mt-5">
                                    <img className='card-img-top mt-5' src={loading} alt="loading" />
                                </div>
                            </div>
                        </div>
                    )
                    : (
                        <div className='countdown container-fluid d-flex flex-column align-items-between justify-content-center'>
                            <div className="row">
                                <div className="col-md-12">
                                    <h2 className='card-title text-center'> Left For My Birthday </h2>
                                </div>
                            </div>
                            <div className="row mt-5 justify-content-around">
                                <InfoBox
                                    info={state.days}
                                    label='Days'
                                />

                                <InfoBox
                                    info={state.hours}
                                    label='Hours'
                                />

                                <InfoBox
                                    info={state.minutes}
                                    label='Minutes'
                                />

                                <InfoBox
                                    info={state.seconds}
                                    label='Seconds'
                                />
                            </div>
                        </div>
                    )
            }
        </>
    )
}

export default Countdown
