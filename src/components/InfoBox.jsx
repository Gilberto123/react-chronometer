import React from 'react'

const InfoBox = (props) => {
    return (
        <div className="col-md-2 info-box">
            <h3 className='text-center'>
                {props.info}
            </h3>
            <h3 className='text-center'>{props.label}</h3>
        </div>
    )
}

export default InfoBox
