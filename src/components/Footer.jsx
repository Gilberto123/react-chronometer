import React from 'react'

const Footer = () => {
    return (
        <div className='nav fixed fixed-bottom footer justify-content-center'>
            <div className="row flex-column">
                <div className="col-md-3">
                    <span className='badge badge-primary'>
                        React App
                    </span>
                </div>
                <div className="col-md-3">
                    <span className="badge badge-secondary">
                        State & Effect Hooks
                    </span>
                </div>
                <div className="col-md-3">
                    <span className="badge badge-success">
                        Bootstrap & Custom styles
                    </span>
                </div>
                <div className="col-md-3">
                    <span className="badge badge-danger">
                        <a href='https://wa.link/in0iwn' target='_blank' rel="noreferrer">
                            By: Ing. José Gilberto Serrano González
                            <i className="fab fa-whatsapp pl-2"></i>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    )
}

export default Footer