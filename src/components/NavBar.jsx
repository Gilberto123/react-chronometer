import React from 'react'

const NavBar = () => {
    return (
        <div className='nav justify-content-center fixed fixed-top'>
            <h1 className='mt-2 text-center'>
                Countdown App
            </h1>
        </div>
    )
}

export default NavBar