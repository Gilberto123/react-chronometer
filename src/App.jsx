import Countdown from './components/Countdown.jsx'
import Footer from './components/Footer.jsx'
import NavBar from './components/NavBar.jsx'
import './styles.css'

function App() {
  return (
    <div className="App">
      <NavBar />
      <Countdown />
      <Footer />
    </div>
  );
}

export default App;
